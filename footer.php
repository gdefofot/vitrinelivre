<footer>
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <a href="#" class="footer-logo mb-1"><b>POZOS</b></a>
                    <ul class="list-inline footer-menu">
                        <li class="list-inline-item">
                            <a href="../index.php">ACCUEIL</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="../pages/commande.php">COMMANDER</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="../pages/galerie.php">GALERIE</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="../pages/contact.php">CONTACT</a>
                        </li>
                    </ul>
                    <p class="copyright-text">Copyright &copy; Georges DEFO (+33)0752790460 - thème :<a href="http://www.Themefisher.com">Themefisher</a>|
                        All right reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Js -->
<script src="../plugins/jquery-2.1.1.min.js"></script>
<script src="../plugins/bootstrap/bootstrap.min.js"></script>
