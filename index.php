<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Accueil</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
    <!-- Fonts -->
    <!-- Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400i|Source+Sans+Pro:300,400,600,700"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,600,700" rel="stylesheet">
    <!-- CSS -->
    <!-- Bootstrap CDN -->
    <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="plugins/themefisher-fonts/themefisher-fonts.css">
    <link rel="stylesheet" href="plugins/magnific-popup/magnific-popup.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- Responsive Stylesheet -->
</head>

<body id="body">
<!-- navigation -->
<header>
    <div class="container">
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand" href="">POZOS</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#">ACCUEIL</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="pages/commande.php">COMMANDE</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="pages/galerie.php">GALERIE</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="pages/contact.php">CONTACT</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    </div>
</header>
<!-- /navigation -->

<!-- hero area -->
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center mb-5 mb-md-0">
                <img class="book_cover" src="images/book_cover.jpg" alt="">
            </div>
            <div class="col-md-6 align-self-center text-center text-md-left">
                <div class="block">
                    <div>
                        <h2 class="font-weight-bold mb-4 font-size-40">PAPA,MAMAN, JE REVIENDRAI DEVELOPPER
                            L'AFRIQUE</h2>
                        <p class="mb-4">L’Afrique est un continent regorgeant de potentiel de par sa diversité
                            culturelle
                            et la richesse de ses terres. Malheureusement, l’histoire ne nous a pas permis d’en jouir
                            pleinement et de mettre cette richesse au service du continent. Nous assistons impuissant
                            au démembrement de notre continent par les multinationales (bras économique armé de
                            l’occident),
                            qui pillent nos ressources avec la complicité de certains de nos dirigeants.
                            <br/>La situation de souffrance générale et de sous emplois des africains les amènent
                            souvent
                            à migrer vers de nouveaux horizons. L’Afrique doit pouvoir compter sur ses fils d’ici et
                            d’ailleurs pour la développer, c’est ma conviction et c’est pour ca que je bats.
                            En dehors des déplacés économiques, nous avons ces jeunes, qui de part leur intelligence
                            ont pu suivre des formations dans les meilleures écoles occidentales. Ces élites ne
                            demandent qu’à aider leur continent, je le sais car j’en fais partie. C’est la raison
                            pour laquelle je me suis lancé dans l’écriture, moi ingénieur de formation. J’ai décidé
                            de léguer aux générations futures des livres édifiants et stimulateurs dans l’optique de
                            leur faire prendre conscience de la lourde tâche qu’ils portent sur les épaules.
                            <br/>C’est dans cette optique que j’ai décidé d’écrire sur l’histoire d’un jeune Africain
                            partant de son pays d’origine pour étudier en occident, et dont le rêve le plus fou est
                            d’apporter son aide au développement de son continent. Le personnage s’appelle Demba NANA,
                            et dans ce premier tome, je plante le décor en vous relatant les réalités de la vie des
                            étudiants en Occident, cette histoire est calquée sur des faits réels vécus par la majorité
                            des africains en en Europe.
                            <br/>Avec ce premier opus, vous aurez une vision de toutes les étapes qu’il a parcouru
                            depuis son pays d’origine jusqu’à l’obtention de son diplôme en France.
                            <br/>Si vous trouvez l’histoire édifiante, n’hésitez pas à parler du livre autour de vous
                            afin que vos proches qui envisagent voyager puisse se le procurer. Cela leur permettra
                            d’avoir un état d’esprit affuté pour affronter les nombreux pièges de l’occident et réaliser
                            leur ambition.
                        </p>
                    </div>
                    <br>
                </div>
            </div>
        </div><!-- .row close -->
        <h2 class="font-weight-bold mb-4 font-size-40">APERCU DU LIVRE</h2>
        <div class="row">
            <div class="col-md-4">
                <div class="previsu_livre">
                    <ul>
                        <li><a data-toggle="modal" data-target="#table-matiere-modal-1">Prévisualiser la table des
                                matières
                                - page 1</a></li>
                        <li><a data-toggle="modal" data-target="#table-matiere-modal-2">Prévisualiser la table des
                                matières
                                - page 2</a></li>
                        <li><a data-toggle="modal" data-target="#presentation-auteur-modal">Prévisualiser la
                                présentation de
                                l'auteur</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="pages/commande.php" class="btn btn-main">Commandez votre exemplaire</a>
            </div>
        </div>
    </div><!-- .container close -->
</section><!-- header close -->

<section class="bg-orange section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="heading">
                    <h2>Le livre plus en détails.</h2>
                </div>
            </div>
            <div class="col-md-6 text-center mb-5 mb-lg-0">
                <div class="content">
                    <div class="titre-video-ligne-1"><h2 class="subheading font-weight-bold mb-10">1- Bienvenue</h2>
                    </div>
                    <iframe class="cadre_video" width="620" height="415"
                            src="https://www.youtube.com/embed/5sk0I5q9SCA">
                    </iframe>
                </div>
            </div>
            <div class="col-md-6 text-center mb-5 mb-lg-0">
                <div class="content">
                    <div class="titre-video-ligne-1"><h2 class="subheading font-weight-bold mb-10">2- Pourquoi tu as
                            écrit ce livre ?</h2></div>
                    <iframe class="cadre_video" width="620" height="415"
                            src="https://www.youtube.com/embed/VxDZVnVWh54">
                    </iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center mb-5 mb-lg-0">
                <div class="content">
                    <div class="content">
                        <div class="titre-video-ligne-2"><h2 class="subheading font-weight-bold mb-10">3- Quelles sont
                                les difficultés rencontrées par de Demba le héros de ton livre ?</h2></div>
                        <iframe class="cadre_video" width="620" height="415"
                                src="https://www.youtube.com/embed/5I3pSMffxRg">
                        </iframe>
                    </div>
                </div>
            </div>
            <div class="col-md-6 text-center mb-5 mb-lg-0">
                <div class="content">
                    <div class="titre-video-ligne-2"><h2 class="subheading font-weight-bold mb-10">4- Quelle a été ta
                            réaction aux commentaires des internautes ?</h2></div>
                    <iframe class="cadre_video" width="620" height="415"
                            src="https://www.youtube.com/embed/VptP7gDqEuQ">
                    </iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-orange section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center mb-5 mb-lg-0">
                <div class="content">
                    <div class="content">
                        <div class="titre-video-ligne-2"><h2 class="subheading font-weight-bold mb-10">5- Pourquoi cette
                                illustration en photo de couverture ?</h2></div>
                        <iframe class="cadre_video" width="620" height="415"
                                src="https://www.youtube.com/embed/mKq8zUki-Pw">
                        </iframe>
                    </div>
                </div>
            </div>
            <div class="col-md-6 text-center mb-5 mb-lg-0">
                <div class="content">
                    <div class="titre-video-ligne-2"><h2 class="subheading font-weight-bold mb-10">6- Pourquoi en
                            Occident faut-il être deux fois meilleur que les natifs ?</h2></div>
                    <iframe class="cadre_video" width="620" height="415"
                            src="https://www.youtube.com/embed/DQK_vFiKEbw">
                    </iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 align-self-center text-center text-md-left">
                <div class="content">
                    <div class="titre-video-ligne-1"><h2 class="subheading font-weight-bold mb-10">7- Quel est ton
                            parcours Dirane ?</h2></div>
                    <iframe class="cadre_video" width="620" height="415"
                            src="https://www.youtube.com/embed/PzYtm9VaX-8">
                    </iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="testimonials section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="heading">
                    <h2>Quelques avis</h2>
                </div>
            </div>
            <div class="mb-lg-0 text-center">
                <div class="testimonial-block">
                    <i class="tf-ion-quote"></i>
                    <p>
                        Ce livret signé par Dirane sonne comme un guide pratique, quelques conseils à l'endroit des
                        candidats à l'immigration française.
                        Bref un ensemble des choses bien à savoir pour mieux s'outiller et éviter certains pièges cachés
                        derrière l'idée de l'eldorado vendue consciemment
                        ou non par des immigrants africains en occident.
                    </p>
                    <div class="author-details">
                        <img src="images/charles.jpeg" alt="">
                        <h4>Charles</h4>
                        <span>Ecrivain</span>
                    </div>
                </div>
            </div>
            <div class="mb-lg-0 text-center">
                <div class="testimonial-block">
                    <i class="tf-ion-quote"></i>
                    <p>
                        L'Afrique est un continent très jeune: dans la plupart des États, l'âge moyen de la population
                        ne dépasse pas 25 ans.
                        Pour cette raison, il est dans notre intérêt de prendre des orientations et des décisions qui
                        participeront au développement de notre continent.
                    </p>
                    <div class="author-details">
                        <img src="images/kevin.jpeg" alt="">
                        <h4>Kevin</h4>
                        <span>Entrepreneur</span>
                    </div>
                </div>
            </div>
            <div class="mb-lg-0 text-center">
                <div class="testimonial-block">
                    <i class="tf-ion-quote"></i>
                    <p>
                        Le livre pose les problèmes de la mal gouvernance en Afrique dont une des conséquences est le
                        départ massif de nombreux jeunes vers l’Occident. L’auteur nous plonge aussi dans les réalités
                        quotidiennes de l’Occident où règnent l’individualisme, les clichés sur l’Afrique, le rejet,
                        l’exclusion et où les jeunes africains font face à la misère noire : petits boulots,
                        tracasseries
                        administratives. Ce qu’on comprend à la lecture de l’ouvrage qui s’apparente à un roman
                        autobiographique c’est que l’Occident n’est qu’un mirage et que le paradis n’existe nulle part.
                        Face à cela, le retour au pays natal est envisagé comme ultime solution. Il vaut mieux retourner
                        en Afrique pour contribuer à son développement. Mais à condition que les pays africains sortent
                        des politiques de la négligence et de l’abandon de leurs propres populations. Au final,
                        l’ouvrage
                        se lit comme un vade-mecum pour celles et ceux qui désirent poursuivre leurs études en Europe
                        notamment en France.
                    </p>
                    <div class="author-details">
                        <img src="images/simon.jpg" alt="">
                        <h4>Simon</h4>
                        <span>Docteur d'université</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="pages/commande.php" class="btn btn-main">Commandez votre exemplaire</a>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <a href="#" class="footer-logo mb-1">POZOS</a>
                    <ul class="list-inline footer-menu">
                        <li class="list-inline-item">
                            <a href="index.php">ACCUEIL</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="pages/commande.php">COMMANDER</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="pages/galerie.php">GALERIE</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="pages/contact.php">CONTACT</a>
                        </li>
                    </ul>
                    <p class="copyright-text">Copyright &copy; Georges DEFO (+33)0752790460 - thème :<a href="http://www.Themefisher.com">Themefisher</a>|
                        All right reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<div id="table-matiere-modal-1" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header entete-sommaire">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Table des matières - Page 1</h4>
            </div>
            <div class="modal-body">
                <img class="images-livre" src="images/table-matieres-1.jpg" alt="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-main btn-footer" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<div id="table-matiere-modal-2" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header entete-sommaire">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Table des matières - Page 2</h4>
            </div>
            <div class="modal-body">
                <img class="images-livre" src="images/table-matieres-2.jpg" alt="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-main btn-footer" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<div id="presentation-auteur-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header entete-sommaire">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Présentation de l'auteur</h4>
            </div>
            <div class="modal-body">
                <img class="images-livre" src="images/quatrieme-couverture.jpg" alt="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-main btn-footer" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<!-- Js -->
<script src="plugins/jquery-2.1.1.min.js"></script>
<script src="plugins/bootstrap/bootstrap.min.js"></script>
<script src="plugins/magnific-popup/jquery.magnific.popup.min.js"></script>
<script src="js/main.js"></script>
</body>

</html>
