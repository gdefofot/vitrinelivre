<?php include('../header.php');?>
<section class="feature-list section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading">
                    <h2>Contactez - nous :</h2>
                </div>
            </div>
        </div>

        <div class="container-contact-form">
            <form id="contact-form" action="contactForm.php" data-toggle="validator">
                <div class="messages"></div>
                <label for="nom">NOM</label>
                <input type="text" id="nom" name="nom" required="required" data-error="Veuillez entrer un nom.">
                <div class="help-block with-errors"></div>

                <label for="prenom">PRENOM</label>
                <input type="text" id="prenom" name="prenom" required="required"
                       data-error="Veuillez entrer un prénom.">
                <div class="help-block with-errors"></div>

                <label for="email" class="label-email">ADRESSE MAIL</label>
                <input type="email" id="email" name="email" required="required"
                       data-error="Une adresse email valide est obligatoire.">
                <div class="help-block with-errors"></div>

                <label for="tel">NUMERO DE TELEPHONE <i class="fa fa-whatsapp icone-whatsapp"></i></label>
                <input type="tel" id="tel" name="tel" data-error="Veuillez entrer un numéro de téléphone valide."
                       placeholder="Sans l'indicatif pays" required="required">
                <div class="help-block with-errors"></div>

                <label for="pays">PAYS</label>
                <select id="pays" name="pays" required="required">
                    <option value="">Selectionnez un pays</option>
                    <?php
                    if (($h = fopen("../pays-indicatifs.csv", "r")) !== FALSE) {
                        while (($data = fgetcsv($h, 1000, ",")) !== FALSE) {
                            echo '<option value="' . $data[1] . ',' . $data[0] . '">' . $data[0] . '</option>';
                        }
                        fclose($h);
                    }
                    ?>
                </select>
                <div class="help-block with-errors"></div>

                <label for="format-livre">FORMAT DU LIVRE</label>
                <select id="format-livre" name="format-livre" required="required">
                    <option value="">Selectionnez un format pour le livre</option>
                    <option class="choix-format" value="e-book">E-Book</option>
                    <option class="choix-format" value="papier">Papier</option>
                </select>
                <div class="help-block with-errors"></div>

                <label for="dedicace-livre">DEDICACE DU LIVRE</label>
                <div class="dedicace-bloc">
                    <div class="dedicace-question"> Souhaitez-vous un livre dédicacé avec un autographe? :</div>
                    <div class="dedicace-checkbox"><input type="checkbox" id="dedicace-livre" name="dedicace-livre"></div>
                </div>

                <div class="dedicace-bloc-message">
                    <label for="dedicace-message">MESSAGE DEDICACE</label>
                    <textarea name="texte-dedicace" id="texte-dedicace"></textarea>
                </div>

                <input class="bouton-envoyer" type="submit" value="Envoyer">
            </form>
        </div>
</section>
<?php include('../footer.php');?>
</body>

<script src="../js/main.js"></script>
</html>

