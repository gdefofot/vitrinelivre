
<?php

require 'vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;

class MailerComponent {

public $mailer = null;
// an email address that will receive the email with the output of the form
public $destinataire = 'pozostablet@gmail.com';
// subject of the email
public $objet = 'Message du formulaire de contact';
// form field names and their translations.
// array variable name => Text to appear in the email
public $fields = array('nom' => 'Nom', 'prenom' => 'Prenom','email' => 'Mail', 'tel' => 'Tel', 'pays' => 'Pays', 'format-livre' => 'Format du livre');
// message that will be displayed when everything is OK :)
public $okMessage = 'Message bien envoyé. Merci, nous revenons vers vous très vite!';
// If something goes wrong, we will display this message.
public $errorMessage = 'Une erreur est survenue. Veuillez réessayer plus tard!';

private $contenuMail;

private $mailExpediteur; 

private $prenomExpediteur;

private $nomExpediteur;

private $indicatifPays;

private $pays;

private $telephone;

private $dedicace;

function __construct()
{
    $this->mailer = new PHPMailer(true);
}

function prepareMailer(){
    if ($this->mailer !== null){
        $mailClient = $this->mailer;
        //Server settings
        $mailClient->SMTPDebug = 0;                                       // Enable verbose debug output
        $mailClient->isSMTP();                                            // Set mailer to use SMTP
        $mailClient->Host       = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
        $mailClient->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mailClient->Username   = $this->destinataire;                    // SMTP username
        $mailClient->Password   = 'FunThNvt7DDDzI4gpLR1';                 // SMTP password
        $mailClient->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
        $mailClient->Port       = 587;                                    // TCP port to connect to
    }                                   
}

function prepareMailText($contenuPost){
    if(count($contenuPost) == 0) throw new \Exception('Form is empty');
    $tab = $this->fields;
    $this->nomExpediteur = $contenuPost['nom'];
    $this->prenomExpediteur = $contenuPost['prenom'];
    $this->mailExpediteur = $contenuPost['email'];
    $this->telephone = $contenuPost['tel'];

    $paysEtIndicatif = explode(",", $contenuPost['pays']);
    $this->indicatifPays = $paysEtIndicatif[0];
    $this->pays = $paysEtIndicatif[1];

    $this->dedicace = $contenuPost['texte-dedicace'];

    $emailText = "Vous avez un nouveau message en provenance du formulaire de contact \n 
                  =================================================================== \n";
    foreach ($contenuPost as $key => $value) {
        if (isset($tab[$key])) {
            if($key ==='tel'){
                $emailText .= "Numéro de Téléphone : (+$this->indicatifPays) $this->telephone\n";
            }else if($key ==='pays'){
                $emailText .= "Pays : $this->pays\n";
            }else{
                $emailText .= "$tab[$key]: $value\n";
            }
        }
    }

    if(isset($contenuPost['dedicace-livre']) && isset($contenuPost['texte-dedicace'])) {
        $emailText .= "Message voulu pour la dédicace: $this->dedicace\n";
    }
    $this->contenuMail = $emailText;
}

function sendMail() {
    try
    {
        //Recipients
        $this->mailer->setFrom('info@example.com', 'Formulaire de contact');
        $this->mailer->addAddress($this->destinataire , 'Pozos Mail Box');     // Add a recipient
        $this->mailer->addReplyTo($this->mailExpediteur, $this->prenomExpediteur.' '.$this->nomExpediteur);

        // Content
        $this->mailer->isHTML(false);                                  // Set email format to HTML
        $this->mailer->Subject = $this->objet;
        $this->mailer->Body  = $this->contenuMail;    
        $this->mailer->send();

        $responseArray = array('type' => 'success', 'message' => $this->okMessage);
    }
    catch (\Exception $e)
    {
        $responseArray = array('type' => 'danger', 'message' => $this->errorMessage);
    }

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $encoded = json_encode($responseArray);
        header('Content-Type: application/json');
        return $encoded;    
    }
}
}