<?php include("../header.php"); ?>
<section class="feature-list section">
<div class="container">
    <?php
    $directory = "../images/galerie";
    $dossiers = scandir($directory);
    foreach ($dossiers as $dossier) {
        // Récupération du nom du dossier de photos pour mettre en titre
        if($dossier !== "." && $dossier !== ".."){
            if(is_dir($directory.'/'.$dossier)){
                echo '
                <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">'.basename($dossier).'</h1>
                <hr class="mt-2 mb-5">
                <div class="row text-center text-lg-left">';

                // Affichage des photos
                $photos = scandir($directory.'/'.$dossier);
                foreach ($photos as $photo){
                    if($photo !== "." && $photo !== ".."){
                        $cheminPhoto =$directory.'/'.$dossier . '/' . $photo;
                        echo '
                        <div class="col-lg-3 col-md-2 col-6">
                            <a href="'.$cheminPhoto.'" target="_blank" class="d-block mb-4 h-100">
                                <img class="img-fluid img-thumbnail" src="'.$cheminPhoto.'" alt="">
                            </a>
                        </div>
                        ';
                    }
                }
                echo '</div>';
            }
        }
    }
    ?>
</section>
<?php include("../footer.php"); ?>
</body>
</html>
