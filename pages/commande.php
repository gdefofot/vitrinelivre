<?php include("../header.php");?>
<section class="feature-list section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading">
                    <h2>Plateformes de distribution :</h2>
                </div>
            </div>
        </div>
        <div class="row mb-40 canal-commande">
            <div class="col-md-6 text-center mb-5 mb-lg-0 logo-amazon">
                <img class="img-fluid logo-livraison" src="../images/amazon.png" alt="">
            </div>
            <div class="col-md-6 align-self-center text-center text-md-left">
                <div class="content">
                    <h4 class="subheading">Amazon.fr</h4>
                    <div class="bloc-commande">Disponible en version papier:</div>
                    <a href="https://amzn.to/358uqa7"
                       class="btn btn-main btn-main-sm" target="_blank"
                       title="amazon.com">Commander</a>

                    <div class="bloc-commande">Disponible en version numérique:</div>
                    <a href="https://amzn.to/2o8hUH8"
                       class="btn btn-main btn-main-sm" target="_blank"
                       title="kindle">Commander</a>
                </div>
            </div>
        </div>
        <div class="row mb-40 canal-commande">
            <div class="col-md-6 order-md-1 order-1 text-center mb-5 mb-lg-0 logo-fnac ">
                <img class="img-fluid logo-livraison" src="../images/fnac.png" alt="">
            </div>
            <div class="col-md-6 order-md-2 order-2 align-self-center text-center text-md-left">
                <div class="content">
                    <h4 class="subheading">Fnac.com</h4>
                    <p>Disponible en version digitale numérique.</p>
                    <a href="http://bit.ly/2Is6Ew9" target="_blank" title="Kobo by fnac"
                       class="btn btn-main btn-main-sm">Commander</a>
                </div>
            </div>
        </div>
        <div class="row mb-40 canal-commande">
            <div class="col-md-6 order-md-1 order-1 text-center mb-5 mb-lg-0 logo-ayoafrika">
                <img class="img-fluid logo-livraison" src="../images/livre-audio.png" alt="">
            </div>
            <div class="col-md-6 order-md-2 order-2 align-self-center text-center text-md-left">
                <div class="content">
                    <h4 class="subheading">Ayokafrica.com</h4>
                    <p>Disponible en version audio.</p>
                    <a href="http://bit.ly/37emJQu" target="_blank" title="Ayokafrica"
                       class="btn btn-main btn-main-sm">Commander</a>
                </div>
            </div>
        </div>
        <div class="row mb-40 canal-commande">
            <div class="col-md-6 order-md-1 order-1 text-center mb-5 mb-lg-0 logo-monde">
                <img class="img-fluid logo-livraison" src="../images/digital-globe-icon-1925.jpg" alt="">
            </div>
            <div class="col-md-6 order-md-2 order-2 align-self-center text-center text-md-left">
                <div class="content">
                    <h4 class="subheading">Livraison dans le monde entier</h4>
                    <p>Disponible en version digitale et en version papier.</p>
                    <a href="contact.php" target="_blank" class="btn btn-main btn-main-sm">Contact</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-orange section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading">
                    <h2>Modes de paiement :</h2>
                </div>
            </div>
        </div>

        <div class="row mb-40 mode-paiement">
            <div class="col-md-6 order-md-1 order-1 text-center mb-5 mb-lg-0 logo-mm">
                <img class="img-fluid img-commande" src="../images/mobile%20money.jpg" alt="">
            </div>
            <div class="col-md-6 order-md-2 order-2 align-self-center text-center text-md-left">
                <div class="content">
                    <h4 class="subheading">Mobile money</h4>
                    <p>Disponible en version digitale et en version papier.</p>
                    <p>Contactez nous par whatsapp de préférence : </p>
                    <span class="num_whatsapp"><p><i class="fa fa-whatsapp icone-whatsapp"></i>+237696970312</p></span>
                    <a href="contact.php" class="btn btn-main-alt btn-main-sm">E-Mail</a>
                </div>
            </div>
        </div>
        <div class="row mb-40 mode-paiement">
            <div class="col-md-6 order-md-1 order-1 text-center mb-5 mb-lg-0 logo-paypal">
                <img class="img-fluid img-commande" src="../images/paypal.png" alt="">
            </div>
            <div class="col-md-6 order-md-2 order-2 align-self-center text-center text-md-left">
                <div class="content">
                    <h4 class="subheading">Paypal</h4>
                    <p>Disponible en version digitale et en version papier.</p>
                    <p>Contactez nous par whatsapp de préférence : </p>
                    <span class="num_whatsapp"><p><i class="fa fa-whatsapp icone-whatsapp"></i>+237696970312</p></span>
                    <a href="contact.php" class="btn btn-main-alt btn-main-sm">E-Mail</a>
                </div>
            </div>
        </div>
        <div class="row mb-40 mode-paiement">
            <div class="col-md-6 order-md-1 order-1 text-center mb-5 mb-lg-0 logo-virement">
                <img class="img-fluid img-commande" src="../images/virement-bancaire.jpg" alt="">
            </div>
            <div class="col-md-6 order-md-2 order-2 align-self-center text-center text-md-left">
                <div class="content">
                    <h4 class="subheading">Virement bancaire</h4>
                    <p>Disponible en version digitale et en version papier.</p>
                    <p>Contactez nous par whatsapp de préférence : </p>
                    <span class="num_whatsapp"><p><i class="fa fa-whatsapp icone-whatsapp"></i>+237696970312</p></span>
                    <a href="contact.php" class="btn btn-main-alt btn-main-sm">E-Mail</a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include("../footer.php");?>
</body>
</html>
