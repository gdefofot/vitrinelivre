
<?php

require 'vendor/autoload.php';
require 'mailer.php';

$mailerComp = new MailerComponent();
$mailerComp->prepareMailer();
$mailerComp->prepareMailText($_POST);
echo $mailerComp->sendMail();